export class Person {
    name: string;
    fantasyName: string;
    cpfCnpj: string;
    phoneList: string[];
  
    constructor() {
      this.name = "";
      this.fantasyName = "";
      this.phoneList = [];
      this.cpfCnpj = "";
    }
  }
  