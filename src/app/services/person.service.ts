import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Person } from '../domain/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor() { }

  private personCountBehaviorSubject = new BehaviorSubject<number>(0);

  setPersonCount(PersonCount = 0){
    this.personCountBehaviorSubject.next(PersonCount);
  }

  getPersonCount() {
    return this.personCountBehaviorSubject;
  }

  getPersonList(): Person[] {
    const personList = JSON.parse(sessionStorage.getItem("personList"));
    const count = personList ? personList.length : 0;
    this.setPersonCount(count);
    return personList ? personList : [];
  }

  addPerson(person: Person): void {
    const personList = this.getPersonList();
    personList.push(person);
    sessionStorage.setItem("personList", JSON.stringify(personList));
  }

  updatePerson(newPerson: Person, oldPerson: Person):void {
    const personList = this.getPersonList();
    const indice = personList.findIndex(({cpfCnpj}) => cpfCnpj === oldPerson.cpfCnpj);
    personList[indice] = newPerson;
    sessionStorage.setItem("personList", JSON.stringify(personList));
  }

  removePerson(person: Person): void {
      const personList = this.getPersonList();
      personList.splice(personList.findIndex(({cpfCnpj}) => cpfCnpj === person.cpfCnpj), 1);
      if (personList.length) {
        sessionStorage.setItem('personList', JSON.stringify(personList));
      } else {
        sessionStorage.removeItem('personList');
      }
  }

}
