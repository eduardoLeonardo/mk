import { Component, EventEmitter, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditModalComponent } from '../edit-modal/edit-modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  @Output() refreshEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output() searchEmitter: EventEmitter<string> = new EventEmitter<string>();
  searchParam: string;

  constructor(private modalService: NgbModal) { }


  search() {
    this.searchEmitter.emit(this.searchParam);
  }

  newPersonModal() {
    const modalRef = this.modalService.open(EditModalComponent,{backdrop:'static', keyboard:false,centered:true });
    modalRef.result.then(() => this.refreshEvent());
  }

  refreshEvent(){
    this.refreshEmitter.emit();
  }

}
