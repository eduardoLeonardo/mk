import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Person } from 'src/app/domain/person';
import { PersonService } from 'src/app/services/person.service';
import { EditModalComponent } from '../edit-modal/edit-modal.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() personList: Person[];
  message: string;
  constructor(private personService: PersonService,private modalService: NgbModal) { }

  ngOnInit(): void {
    this.refresh();
  }

  
  refresh(): void {
    this.personList = this.personService.getPersonList();
    if(!this.personList.length) {
      this.searchResultMessage('Nenhum cadastro foi criado ainda.');
    }
  }

  search(param: string):void {
    this.refresh();
    if(param) {
      this.personList = this.personList.filter( person => person.name.includes(param));
      if(this.personList && this.personList.length > 0) {
        this.searchResultMessage('Nenhum resultado para essa busca');
      }
    } 
  }

  edit(person: Person): void {
    const modalConfirmRef = this.modalService.open(EditModalComponent,{backdrop:'static', keyboard:false,centered:true });
    modalConfirmRef.componentInstance.personUpdate = person;
    modalConfirmRef.componentInstance.title="Editar";
    modalConfirmRef.result.then( (result) =>  {
      if(result) {
        this.refresh();
      }
    });
  }

  exclude(person: Person): void {
    this.personService.removePerson(person);
    this.refresh();
  }

  searchResultMessage(message: string): string {
    return this.message = message;
  }

  getMask(cpfCnpj: string): string {
    return cpfCnpj.length < 12 ? '000.000.000-00' : '00.000.000/0000-00';
  }
}
