import { Component, Injectable, Input, OnInit} from '@angular/core';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Person } from 'src/app/domain/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})
@Injectable()
export class EditModalComponent implements OnInit{

  @Input() set personUpdate({name, fantasyName, phoneList, cpfCnpj}: Person){
    this.newPerson.name = name;
    this.newPerson.fantasyName = fantasyName;
    this.newPerson.phoneList = [...phoneList];
    this.newPerson.cpfCnpj = cpfCnpj;
    this.oldPerson = {name, fantasyName, phoneList, cpfCnpj};
  }

  @Input() title = 'Novo';
  private oldPerson: Person;
  phone=''
  phoneList = [];
  newPerson = new Person();
  constructor(public editPersonModal: NgbActiveModal, private personService: PersonService) {
  }

  ngOnInit() {
   
  }
  
  save(): void {
    if(this.saveIsValid()) {
      if (this.oldPerson) {
        this.personService.updatePerson(this.newPerson,this.oldPerson);
      } else {
        this.personService.addPerson(this.newPerson);
      }
      this.editPersonModal.close(this.newPerson);
    }
  }

  saveIsValid(): boolean {
    const {name, cpfCnpj} = this.newPerson;
     return  name !== '' && cpfCnpj !== '';
  }

  getMask(cpfCnpj: string): string {
    return cpfCnpj.length < 12 ? '000.000.000-00' : '00.000.000/0000-00';
  }

  addContact() {
    if(this.phone != '' && this.phone.length == 11) {
      this.newPerson.phoneList.push(this.phone);
      this.phone = '';
    }
  }

  excludeContact(phone: string): void {
    const index = this.newPerson.phoneList.indexOf(phone);
    this.newPerson.phoneList.splice(index,1);
  }

}
