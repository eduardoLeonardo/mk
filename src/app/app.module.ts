import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ListComponent } from './components/list/list.component';
import { FormsModule } from '@angular/forms';
import { PersonService } from './services/person.service';
import { EditModalComponent } from './components/edit-modal/edit-modal.component';
import { IConfig, NgxMaskModule } from 'ngx-mask';

const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListComponent,
    EditModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxMaskModule.forRoot(maskConfig),
  ],
  providers: [PersonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
